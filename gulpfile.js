let gulp = require('gulp'),
    scss = require('gulp-sass'),
    rigger = require('gulp-rigger'),
    browserSync = require('browser-sync').create(),
    del = require('del'),
    tinypng = require('gulp-tinypng'),
    concat = require('gulp-concat'),
    minify = require('gulp-minify');

async function scssFunc() {
    return gulp.src('app/style/main.scss')
        .pipe(scss())
        .pipe(gulp.dest('public/style'))
}

async function htmlFunc() {
    gulp.src('app/index.html')
        .pipe(rigger())
        .pipe(gulp.dest('public'));
}

function delFunc() {
    return del(['public/*'])
}

function watchFunc() {
    browserSync.init({
        server: {
            baseDir: 'public'
        }
    });
    gulp.watch(['./app/style/*.scss', './app/style/*/*.scss'], scssFunc).on('change', browserSync.reload);
    gulp.watch(['./app/index.html', './app/source/*.html'], htmlFunc).on('change', browserSync.reload);
    gulp.watch('./app/js/*.js', concatJS).on('change', browserSync.reload);
}

async function img() {
    gulp.src('app/img/**')
        .pipe(gulp.dest('public/img'))
}

async function favicon() {
    gulp.src('app/img/*.ico')
        .pipe(gulp.dest('public'))
}

async function compressImg(){
        gulp.src('app/img/*.png')
            .pipe(tinypng('JScTzrhH0qUDgimqS4Zbja7HwhMyLuzS'))
            .pipe(gulp.dest('public/img'));
}

async function jsFunc() {
    return gulp.src('app/js/*.js')
        .pipe(gulp.dest('public/js'));

}

async function concatJS() {
    return gulp.src('app/js/*.js')
        .pipe(concat('script.js'))
        .pipe(gulp.dest('public/js'));
}

async function compressJS() {
    return gulp.src('public/js/script.js')
        .pipe(minify())
        .pipe(gulp.dest('public/js'));
}

gulp.task('scss', scssFunc);

gulp.task('html', htmlFunc);

gulp.task('delete', delFunc);

gulp.task('watch', watchFunc);

gulp.task('img', img);

gulp.task('favicon', favicon);

gulp.task('compressImg', compressImg);

gulp.task('js', jsFunc);

gulp.task('concatJS', concatJS);

gulp.task('compressJS', compressJS);



