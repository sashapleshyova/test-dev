var viewMoreButton = document.getElementsByClassName('dr-osvaldo__view-more_js')[0];
var text = document.getElementsByClassName('dr-osvaldo__text_js')[0];

viewMoreButton.onclick = function () {
    text.classList.toggle('dr-osvaldo__text_all');
    if (text.classList.contains('dr-osvaldo__text_all')) {
        viewMoreButton.innerHTML = 'Hide';
    }
    else {
        viewMoreButton.innerHTML = 'View more';
    }
};


var servicesButtons = document.getElementsByClassName('services__card-title_js');
var servicesCards = document.getElementsByClassName('services__card_js');

for (let i=0; i<servicesButtons.length; i++){
    servicesButtons[i].onclick = function () {
        servicesCards[i].classList.toggle('services__card_open');
    }
}


window.addEventListener('load', function () {
    var testimonialsTexts = document.getElementsByClassName('testimonials__card-text_js');
    var testimonialsShowMore = document.getElementsByClassName('testimonials__show-more_js');
    var testimonialsCards = document.getElementsByClassName('testimonials__slider-card_js');

    function makeTexts() {
        let texts = [];
        for (let i=0; i<testimonialsTexts.length; i++){
            let keep = testimonialsTexts[i].innerHTML;
            if (testimonialsTexts[i].scrollHeight > testimonialsTexts[i].clientHeight){
                testimonialsShowMore[i].classList.add('visible');
                while(testimonialsTexts[i].scrollHeight > testimonialsTexts[i].clientHeight) {
                    testimonialsTexts[i].innerHTML = testimonialsTexts[i].innerHTML.substring(0, testimonialsTexts[i].innerHTML.lastIndexOf(' '));
                    testimonialsTexts[i].innerHTML = testimonialsTexts[i].innerHTML + "...";
                }
            }
            texts.push({
                long: keep,
                short: testimonialsTexts[i].innerHTML
            })
        }
        return texts;
    }

    var innerTexts = makeTexts();

    for (let i=0; i<testimonialsTexts.length; i++){
        testimonialsShowMore[i].onclick = function () {
            if (testimonialsTexts[i].classList.contains('testimonials__card-text_all')){
                testimonialsTexts[i].innerHTML = innerTexts[i].short;
                testimonialsShowMore[i].innerHTML = 'view more';
            }
            else {
                testimonialsTexts[i].innerHTML = innerTexts[i].long ;
                testimonialsShowMore[i].innerHTML = 'hide';
            }
            testimonialsTexts[i].classList.toggle('testimonials__card-text_all');
            testimonialsCards[i].classList.toggle('testimonials__card_long');
        }
    }
});

