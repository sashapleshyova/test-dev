var button = document.getElementById('header__button-open-menu_js');
var lines = document.getElementsByClassName('header__cross-line_js');
var menu = document.getElementById('header__mobile-menu_js');

var links = document.getElementsByClassName('header__link_js');

button.onclick = function () {
        for (let i=0; i<3; i++){
            lines[i].classList.toggle('_open');
        }
        menu.classList.toggle('_open');
};

for (var i=0; i<links.length; i++){
    links[i].onclick = function () {
        for (let i=0; i<3; i++){
            lines[i].classList.remove('_open');
        }
        menu.classList.remove('_open');
    }
}
