

window.addEventListener('load', function () {

    var sliderContent = document.getElementById('testimonials__slider-content_js');

    var numberOfSlides = sliderContent.children.length;

    var checked = 0;

    var cardWidth = document.getElementsByClassName('testimonials__slider-card_js')[0].clientWidth;

    var marginLeft = checked * cardWidth;

    var toRight = document.getElementById('testimonials__arrow-to-right_js');
    var toLeft = document.getElementById('testimonials__arrow-to-left_js');
    var navBar = document.getElementById('testimonials__navigation_js');
    var points = document.getElementById('testimonials__navigation-points_js').children;

    sliderContent.style.marginLeft = marginLeft + 'px';

    isVisibleNavigation();
    createPoints();
    onClickPoint();

    var startX, endX;


    sliderContent.addEventListener('touchstart', function (e) {
        startX = e.changedTouches[0].clientX;
    }, false);
    sliderContent.addEventListener('touchend', function (e) {
        endX = e.changedTouches[0].clientX;
        if (Math.abs(startX - endX) >0) {
            if (startX - endX < 0 && checked > 0) {
                points[checked].classList.remove('checked');
                checked--;
                points[checked].classList.add('checked');
                changeContentMargin();
                checkBorder();
            } else if (startX - endX > 0 && checked < numberOfSlides - 1) {
                points[checked].classList.remove('checked');
                checked++;
                points[checked].classList.add('checked');
                changeContentMargin();
                checkBorder();
            }
        }
    }, false);


    toRight.onclick = function () {
        points[checked].classList.remove('checked');
        ++checked;
        points[checked].classList.add('checked');
        changeContentMargin();
        checkBorder();
    };

    toLeft.onclick = function () {
        points[checked].classList.remove('checked');
        --checked;
        points[checked].classList.add('checked');
        changeContentMargin();
        checkBorder();
    };

    function isVisibleNavigation() {
        if (numberOfSlides === 1) {
            navBar.classList.add('invisible');
        } else {
            navBar.classList.remove('invisible');
        }
    }

    function checkBorder() {
        if (checked <= 0) {
            toLeft.classList.add('testimonials__arrow_no-active');
        } else {
            toLeft.classList.remove('testimonials__arrow_no-active');
        }

        if (checked >= numberOfSlides - 1) {
            toRight.classList.add('testimonials__arrow_no-active');
        } else {
            toRight.classList.remove('testimonials__arrow_no-active');
        }
    }

    function createPoints() {
        var pointsContainer = document.getElementById('testimonials__navigation-points_js');
        for (let i = 0; i < numberOfSlides; i++) {
            let point = document.createElement('div');
            point.classList.add('testimonials__navigation-point');
            pointsContainer.appendChild(point);
        }
        pointsContainer.children[checked].classList.add('checked')
    }

    function changeContentMargin() {
        marginLeft = checked * (cardWidth * (-1) - 40);
        sliderContent.style.marginLeft = marginLeft + 'px';
    }

    function onClickPoint() {
        for (let i = 0; i < points.length; i++) {
            points[i].onclick = function () {
                points[checked].classList.remove('checked');
                checked = i;
                points[checked].classList.add('checked');
                checkBorder();
                changeContentMargin();
            }
        }
    }




}, false);


